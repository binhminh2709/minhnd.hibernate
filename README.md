minhnd.hibernate
================
ojdbc14.jar: được biên dịch và đóng gói bởi Java phiên bản 1.4
ojdbc6.jar: được biên dịch và đóng gói bởi Java phiên bản 6.

// Driver class:
oracle.jdbc.driver.OracleDriver
 
// URL Connection String: (SID)
String urlString ="jdbc:oracle:thin:@myhost:1521:mysid"
 
 
// URL Connection String:  (Service Name)
String urlString ="jdbc:oracle:thin:username/pass@//myhost:1521/myservicename"
 
// Or:
String urlString ="jdbc:oracle:thin:@myhost:1521/myservicename";

// Driver class:
com.mysql.jdbc.Driver
 
// URL Connection String:
String url = "jdbc:mysql://hostname:3306/dbname";
 
 
// Ví dụ:
String url = "jdbc:mysql://localhost:3306/simplehr";


Thư viện điều khiển Database SQLServer (JTDS)
JTDS là một thư viện khác điều khiển database SQLServer, nó là một thư viện mã nguồn mở.
jTDS: là một mã nguồn mở thuần Java 100% (type 4) 3,0 JDBC điều khiển cho Microsoft SQL Server (6,5, 7, 2000, 2005, 2008, 2012)
và Sybase ASE (10, 11, 12, 15). jTDS dựa trên FreeTDS và hiện là trình điều khiển JDBC nhanh nhất cho SQL Server và Sybase.
jTDS là 100% tương thích với JDBC 3.0, hỗ trợ  forward-only and scrollable/updateable ResultSet và
thực hiện tất cả các methodDatabaseMetaData và ResultSetMetaData.

// Driver Class
net.sourceforge.jtds.jdbc.Driver
 
// Connection URL String:
jdbc:jtds:<server_type>://<server>[:<port>][/<database>][;<property>=<value>[;...]]
 
// Example 1:
String url = "jdbc:jtds:sqlserver://MYPC:1433/simplehr;instance=SQLEXPRESS;user=sa;password=s3cr3t";
 
getConnection(url);
 
// Example 2:
String url = "jdbc:jtds:sqlserver://MYPC:1433/simplehr;instance=SQLEXPRESS";
 
getConnection(url, "sa", "s3cr3t"):


