package com.usetheindexluke;

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.Persistence;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Predicate;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class DynamicSQLJPA {
  public static void main(String[] args) {
    EntityManager em = Persistence.createEntityManagerFactory("usetheindexluke").createEntityManager();
    try {
      Integer subsidiaryId = null, employeeId = null;
      String lastName = "Winand";
      
      CriteriaBuilder queryBuilder = em.getCriteriaBuilder();
      CriteriaQuery<Employees> query = queryBuilder.createQuery(Employees.class);
      Root<Employees> r = query.from(Employees.class);
      List<Predicate> predicates = new ArrayList<Predicate>();
      if (lastName != null) {
        predicates.add(queryBuilder.equal(queryBuilder.upper(r.get(Employees_.lastName)), lastName.toUpperCase()));
      }
      if (employeeId != null) {
        predicates.add(queryBuilder.equal(r.get(Employees_.employeeId), employeeId));
      }
      if (subsidiaryId != null) {
        predicates.add(queryBuilder.equal(r.get(Employees_.subsidiaryId), subsidiaryId));
      }
      
      query.where(predicates.toArray(new Predicate[0]));
      
      TypedQuery<Employees> q = em.createQuery(query);
      List<Employees> result = q.getResultList();
      for (Iterator<Employees> it = result.iterator(); it.hasNext();) {
        Employees e = it.next();
        System.out.println("Id " + e.getEmployeeId() + ": " + e.getFirstName() + " " + e.getLastName());
      }
    } finally {
      em.close();
    }
  }
}
