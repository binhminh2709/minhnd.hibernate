package com.usetheindexluke;

/**
 * To limit the selected columns in a join with JP-QL "new SalesHeadDTO()"
 * http://use-the-index-luke.com/sql/join/hash-join-partial-objects
 */

/*
 * "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License.
 * 
 * http://Use-The-Index-Luke.com/
 */

public class EmployeesHeadDTO implements EmployeesHeadInterface {
  private String firstName;
  private String lastName;
  
  public EmployeesHeadDTO(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  public String getFirstName() {
    return firstName;
  }
  
  public String getLastName() {
    return lastName;
  }
}
