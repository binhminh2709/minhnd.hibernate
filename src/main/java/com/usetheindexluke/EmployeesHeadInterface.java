package com.usetheindexluke;

/**
 * To limit the selected columns in a join with JP-QL "new SalesHeadDTO()" but
 * stay compatible with the full Entity.
 * http://use-the-index-luke.com/sql/join/hash-join-partial-objects
 */

/*
 * "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License.
 * 
 * http://Use-The-Index-Luke.com/
 */

public interface EmployeesHeadInterface {
  public String getFirstName();
  
  public String getLastName();
}
