package com.usetheindexluke;

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import java.util.List;

public class JoinJPA {
  public static void main(String[] args) {
    EntityManager em = Persistence.createEntityManagerFactory("usetheindexluke").createEntityManager();
    
    try {
      CriteriaBuilder queryBuilder = em.getCriteriaBuilder();
      CriteriaQuery<Employees> query = queryBuilder.createQuery(Employees.class);
      Root<Employees> r = query.from(Employees.class);
      query.where(queryBuilder.like(queryBuilder.upper(r.get(Employees_.lastName)), "WIN%"));
      
      /*
       * // Uncomment for join fetch r.fetch("sales", JoinType.LEFT);
       * query.distinct(true); // needed to avoid duplication of Employee
       * records
       */
      
      List<Employees> emp = em.createQuery(query).getResultList();
      for (Employees e : emp) {
        // System.out.println("Id " + e.hashCode() + " (" + e.getSubsidiaryId()
        // + "): " + e.getFirstName() + " " + e.getLastName());
        for (Sales s : e.getSales()) {
          // System.out.println("   SalesId: " + s.getSaleId()
          // + " date: " + s.getSaleDate()
          // + " EUR value: " + s.getEurValue());
        }
      }
    } finally {
      em.close();
    }
  }
}
