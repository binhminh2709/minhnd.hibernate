package com.usetheindexluke;

/**
 * To limit the selected columns in a join with JP-QL "new SalesHeadDTO()"
 * http://use-the-index-luke.com/sql/join/hash-join-partial-objects
 */

/* "Use The Index, Luke" by Markus Winand is licensed under a Creative Commons
 * Attribution-Noncommercial-No Derivative Works 3.0 Unported License. 
 * 
 *                     http://Use-The-Index-Luke.com/
 */

import java.math.BigDecimal;
import java.util.Date;

public class SalesHeadDTO implements SalesHeadInterface {
  private Date saleDate;
  private BigDecimal eurValue;
  private EmployeesHeadDTO employee;
  
  public SalesHeadDTO(Date saleDate, BigDecimal eurValue, String firstName, String lastName) {
    this.saleDate = saleDate;
    this.eurValue = eurValue;
    this.employee = new EmployeesHeadDTO(firstName, lastName);
  }
  
  public Date getSaleDate() {
    return saleDate;
  }
  
  public BigDecimal getEurValue() {
    return eurValue;
  }
  
  public EmployeesHeadDTO getEmployee() {
    return employee;
  }
}
