package entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


//@Entity chú thích một class là một Entity
//Phần tử (element) name của @Entity là không bắt buộc.
//Việc chỉ định rõ name của @Entity cho phép viết ngắn câu HSQL
@Entity
@Table(name = "ACCOUNT")
public class Account implements Serializable {
	
	private Branch openBranch;
  
  // Phần tử foreignKey giúp chỉ rõ tên Foreign Key trong DB.
  // Điều này sẽ giúp Hibernate tạo ra DB từ các Entity java một cách chính xác hơn.

	//Phần tử fetch có 2 giá trị
	//1 - FetchType.LAZY
	//2 - FetchType.EAGER
   @ManyToOne(fetch = FetchType.LAZY)
   //@JoinColumn(name = "OPEN_BRANCH_ID", nullable = false, foreignKey = @ForeignKey(name = "ACCOUNT_BRANCH_FK"))
   @JoinColumn(name = "OPEN_BRANCH_ID", nullable = false, foreignKey = @ForeignKey(name = "ACCOUNT_BRANCH_FK"))
   public Branch getOpenBranch() {
       return openBranch;
   }
}
