package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//@Table cho phép chú thích tên bảng
//Các giàng buộc duy nhất trong bảng.
//Phần tử name không bắt buộc.
//Nếu bạn không chỉ rõ tên bảng trong phần tử name ...
//.. Hibernate sẽ dựa vào phần tử name của @Entity sau đó mới
//tới tên của class.

@Table(name = "invoice_header", uniqueConstraints = @UniqueConstraint(columnNames = { "invoice_num" }))
@Entity
public class InvoiceHeader implements java.io.Serializable {
	
	private String invoiceNum;
	
	@Column(name = "invoice_num", nullable = false, length = 20)
	public String getInvoiceNum() {
		return this.invoiceNum;
	}
}
