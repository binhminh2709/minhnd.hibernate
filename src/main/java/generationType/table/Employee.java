package generationType.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Employee {
	
	private Long empId;

	@TableGenerator(
			name = "empGen",
			table = "ID_GEN_TABLE",
			pkColumnName = "KEY_COLUMN",
			valueColumnName = "VALUE_COLUMN",
			pkColumnValue = "EMP_ID",
			allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "empGen")
	public Long getEmpId() {
		return empId;
	}
	
	//Chú ý: Bạn có thể tùy biến tên bảng, tên cột (ID_GEN_TABLE, KEY_COLUMN, VALUE_COLUMN)
}
