package generator;

import java.io.File;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class SchemaGeneratorDemo {
	public static void main(String[] args) {
		String configFileName = "hibernate-oracle.cfg.xml";
		
		String outFile = "script-output.sql";
		
		// Tạo đối tượng cấu hình.
		Configuration configuration = new Configuration();
		// Mặc định nó sẽ đọc cấu hình trong file hibernate.cfg.xml
		// Bạn có thể chỉ định rõ file nếu muốn:
		//
		configuration.configure(configFileName);
		
		// Tạo đối tượng SchemaExport.
		SchemaExport se = new SchemaExport(configuration);
		
		// Ngừng nếu có gì đó lỗi xẩy ra.
		boolean haltOnError = false;
		se.setHaltOnError(haltOnError);
		
		// Các SQL script sẽ được ghi vào đây.
		String path = "C:/simplehr/" + outFile;
		File file = new File(path);
		
		// Tạo thư mục nếu chưa có.
		file.getParentFile().mkdirs();
		
		System.out.println("Generate Script to: " + path);
		
		// Các lệnh SQL ngăn cách nhau bởi dấu chấm phẩy.
		se.setDelimiter(";");
		se.setOutputFile(path);
		
		// Ghi script tạo bảng,.. ra file.
		boolean script = true;
		// Thực sự thực thi vào DB hay không.
		// Đòi hỏi bạn cần có Database thực sự.
		// (Oracle, MySQL, SQLServer..)
		boolean export = false;
		// Generate
		se.create(script, export);
		
	}
}
