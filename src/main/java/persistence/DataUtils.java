package persistence;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import common.Department;
import common.Employee;

public class DataUtils {
	
	public static Department findDepartment(Session session, String deptNo) {
		Criteria crit = session.createCriteria(Department.class);
		crit.add(Restrictions.eq("deptNo", deptNo));
		return (Department) crit.uniqueResult();
	}
	
	public static Long getMaxEmpId(Session session) {
		String sql = "Select max(e.empId) from " + Employee.class.getName() + " e ";
		Query query = session.createQuery(sql);
		Long value = (Long) query.uniqueResult();
		if (value == null) {
			return 0L;
		}
		return value;
	}
	
	public static Employee findEmployee(Session session, String empNo) {
		Criteria crit = session.createCriteria(Employee.class);
		crit.add(Restrictions.eq("empNo", empNo));
		return (Employee) crit.uniqueResult();
	}
}
