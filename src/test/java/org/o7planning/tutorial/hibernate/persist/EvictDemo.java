package org.o7planning.tutorial.hibernate.persist;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;

import persistence.DataUtils;
import persistence.HibernateUtils;

import common.Employee;

public class EvictDemo {
	
	//@Test
	public void testEvict() {
		SessionFactory factory = HibernateUtils.getSessionFactory();
		
		Session session = factory.getCurrentSession();
		Employee emp = null;
		try {
			session.getTransaction().begin();
			
			// Đây là một đối tượng có tình trạng Persistent.
			emp = DataUtils.findEmployee(session, "E7499");
			
			// ==> true
			System.out.println("- emp Persistent? " + session.contains(emp));
			
			// Sử dụng evict(Object) để đuổi đối tượng Persistent
			// ra khỏi quản lý của Hibernate.
			session.evict(emp);
			
			// Lúc này 'emp' đang có trạng thái Detached.
			// ==> false
			System.out.println("- emp Persistent? " + session.contains(emp));
			
			// Tất cả các thay đổi trên 'emp' sẽ không được update
			// nếu không đưa 'emp' trở lại trạng thái Persistent.
			emp.setEmpNo("NEW");
			
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
	}
}
